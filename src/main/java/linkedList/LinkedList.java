package linkedList;

import java.io.IOException;
import java.io.InputStreamReader;

public class LinkedList {
	
	private class Node{
		private double value;
		private Node next;
		private Node(double value, Node next){
			this.value = value;
			this.next = next;
		}
		private Node getNext(){
			return this.next;
		}
		
		private boolean hasNext(){
			return this.next != null;
		}
		
		private double getValue(){
			return value;
		}
	}
		
	private Node head;
			
	public LinkedList() {
		this("");
	}
	
	public LinkedList(String string) {
		initializeList(string);	
	}

	public LinkedList(InputStreamReader in) throws IOException {
		this(getString(in));
	    
	}
	
	private void initializeList(String string) {
		String[] values;
		if (string.length() == 0)
			head = null;
		else{	
			values = string.split(" ");
			for (String val : values) {
				this.addLast(Double.parseDouble(val));
			}
		}
	}

	private static String getString(InputStreamReader in) throws IOException {
		StringBuilder str = new StringBuilder();
		int data = in.read();
	    while(data != -1) {
	    	str.append((char) data);
	        data = in.read();
	    }
	    return str.toString();
	}

	public boolean isEmpty() {
		
		return head == null;
	}

	public int length() {
		int counter = 0;
		Node next = head;
		while (next != null) {
			counter++;
			next = next.getNext();			
		}
		return counter;
	}
	
	public void addFirst(double d) {
		Node newHead = new Node (d, head);
		head = newHead;		
	}
	
	public void addLast(double i) {
		Node cur = head;
		if (cur == null)
			head = new Node (i, null);
		else{
			while(cur.hasNext()){
				cur = cur.getNext();	
			}
			cur.next = new Node(i, null);
		}
		
	}
	
	public String toString(){
		StringBuilder list = new StringBuilder();
		list.append("[");
		Node cur = head;
		for (int i = 0; i < this.length() - 1; i++) {
			list.append(cur.getValue() + " ");
			cur = cur.getNext();
		}
		if (cur != null)
			list.append(cur.getValue());
		list.append("]");
		return list.toString();
	}

	public boolean removeFirst() {
		if (head == null)
			return false;
		head = head.getNext();
		return true;
	}

	public boolean removeLast() {
		Node last;
		if(head == null)
			return false;
		if (!head.hasNext()){
			head = null;
			return true;
		}
		last = head;
		while(last.getNext().hasNext())
			last = last.getNext();
		last.next = null;
		return true;
			
	}

	public boolean remove(double d) {
		Node cur = head;
		Node prev = null;
		if(cur == null)
			return false;
		while(cur != null && cur.getValue() != d){
			prev = cur;
			cur = cur.getNext();
		}
		if(cur == null)
			return false;
		if(prev == null){
			head = head.getNext();
			return true;
		}
		prev.next = cur.next;
		cur.next = null;
		return true;
	}

	public double mean() {
		double sum = 0;
		int counter = 0;
		Node next = head;
		while (next != null) {
			counter++;
			sum += next.getValue();
			next = next.getNext();			
		}
		if (sum == 0)
			return 0;
		return sum/counter;
	}

	public double stdDev() {
		double sum = 0;
		double mean = this.mean();
		int length = this.length();
		Node next = head;
		while (next != null) {
			sum += Math.pow((next.getValue() - mean), 2);
			next = next.getNext();			
		}
		if(length < 2)
			return 0;
		return Math.sqrt(sum/(length - 1));
		
	}

	
		
	

}
